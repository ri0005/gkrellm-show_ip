/*
 * gkrellm2 plugin: show_ip
 *
 * Copyright (c) 2007 by Detlef Wien <detlefwien@gmx.de>
 *
 * Based on a plugin by Luksch Klaus-J. <cyberclaus@gmx.net>
 *
 * This program is free software which I release under the GNU General Public
 * License. You may redistribute and/or modify this program under the terms
 * of that license as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.  Version 2 is in the
 * COPYRIGHT file in the top level directory of this distribution.
 *
 * To get a copy of the GNU General Puplic License, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 */

/*
    V1.1   April 2007    (Detlef Wien)

    - initial release

*/

#include <gkrellm2/gkrellm.h>

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <gtk/gtk.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <net/if.h>
#include <arpa/inet.h>

#define PLUGIN_NAME		"GKrellM2-show_ip"
#define PLUGIN_VERSION		"1.1"
#define PLUGIN_URL		"http://"
#define PLUGIN_PLACEMENT	(MON_UPTIME | MON_INSERT_AFTER)
#define CONFIG_NAME		"show_ip"
#define CONFIG_KEY		"show_ip"
#define STYLE_NAME		"show_ip"

#define TIMESTAMP_SIZE		32

#define E_SOCKET		"Socket error!"
#define E_INTERFACE		"---.---.---.---"

#define inaddrr(x) (*(struct in_addr *) &ifr->x[sizeof sa.sin_port])
#define IFRSIZE   ((int)(size * sizeof (struct ifreq)))

static GkrellmMonitor	*monitor;
static GkrellmPanel	*panel;
static GkrellmDecal	*decal_text;
static GkrellmTicks	*gticks;
static GtkWidget	*interface_entry = NULL;
static GtkWidget	*prefix_entry    = NULL;
static GtkWidget    *prefix_btn	 = NULL;
static GtkWidget    *interface_btn	 = NULL;
static GtkWidget    *scroll_btn	 = NULL;

static gchar		*strg_iface;
static gchar		*strg_prefix;
static int		int_iface     = FALSE;
static int		int_prefix    = FALSE;
static int		int_scroll    = FALSE;
int _asprintf(
    char **  const string_pointer,
    const char *  const format,
    ...);
void clearcache(void);

static gint		style_id;
static gchar		*display_text;
static gchar        *plugin_info_text[] =
{
  "\n",
  "<b> GKrellM2 show ip\n\n",
  " This plugin shows the actual ip address of a given network interface.\n",
  "   - Surprise! Did you expect that? ;)\n\n",
  " Why use this plugin ?\n",
  " If you are behind a firewall with NAT or in a LAN you can see youre\n",
  " local address (not the external IP). This is usefull too for\n",
  " wireless APs with DHCP enabled ...\n\n",
  "<b> Usage:\n\n",
  " Left clicking the plugin toggles the scroll.\n",
  " Right clicking the plugin opens the configuration.\n\n",
  "<b> Configuration:\n\n",
  "<i>   Interface field\n",
  "      Enter in the interface entryfield the name of\n",
  "      the interface you want to watch.\n"
  "      (eth0, eth1, irda0, ppp0, dsl0, wlan0, or any other ...)\n\n",
  "<i>   Prefix field\n",
  "      Enter in the prefix entryfield any string you\n",
  "      want to see in front of the displayed string.\n",
  "      (one word, special chars may work but no spaces) \n\n",
  "<i>   Interface checkbox\n",
  "      Display the Interface name, or not.\n\n",
  "<i>   Prefix checkbox\n",
  "      Display the prefix, or not.\n\n",
  "<i>   Scroll text checkbox\n",
  "      Let the text scrolling, or not.\n",
  "      Left clicking the plugin toggles the scroll too.\n\n",
  "<b> IP Address Export:\n\n",
  "The IP address for net interfaces can be exported for display on\n",
  "a builtin Net monitor chart by entering the $A substitution variable\n",
  "in the Net->Setup 'Format String for Chart Labels'\n",
  "\n",
  "When exporting like this, the GkrellM2 show ip panel may be hidden\n",
  "by clearing the 'Interface' entry and unchecking the 'show interface\n",
  "name' and 'show prefix' check buttons.\n",
  "\n",
  "<b> Copyright 2007 by Detlef Wien <DetlefWien@GMX.de>\n\n",
  " This program is free software; you can redistribute it and/or modify\n",
  " it under the terms of the GNU General Public License as published by\n",
  " the Free Software Foundation; either version 2 of the License, or\n",
  " (at your option) any later version.\n\n",
  " This program is distributed in the hope that it will be useful,\n",
  " but WITHOUT ANY WARRANTY; without even the implied warranty of\n",
  " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n",
  " GNU General Public License for more details.\n\n",
  " You should have received a copy of the GNU General Public License\n",
  " along with this program; if not, write to the Free Software\n",
  " Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n\n",
  "\n",
  "Modified by Robert Izzard to monitor, alternately, the LAN and WAN IP \n",
  "addresses. Released under the above GNU GPL2.\n",
};

// RGI : external IP command
static char * external_data(char * command);
#define BUFSIZE 25
/* update every UPDATE_TIME loops of the command list,
 * or when IP_LOCAL changes */
#define UPDATE_TIME 20
//#define REMOTE_CURL_COMMAND "timeout 1 curl -s ifconfig.me"
#define REMOTE_CURL_COMMAND "timeout 1 curl -s https://ipinfo.io/"
#define Dprint(...) /* debug: do nothing */
#define COMMANDS_LIST "local ip","ip","hostname","org"
#define LABELS_LIST "L:","W:","",""
#define REDIRECT "2>/dev/null"
#define NCOMMANDS (sizeof(commands)/sizeof(commands[0]))
static char * labels[] = { LABELS_LIST };
static char * commands[] = { COMMANDS_LIST };
char * cache[NCOMMANDS] = { NULL };

static void read_ip(char *buffer_,
                    size_t bufsz_,
                    char *interface,
                    int n);
static void read_ip(char *buffer_,
                    size_t bufsz_,
                    char *interface,
                    int n)
{
    struct ifreq ifr;
    struct sockaddr_in *sa = (struct sockaddr_in *)&ifr.ifr_addr;
    static int count = 0;

    Dprint("run command %d, count %d\n",n,count);
    if(count%UPDATE_TIME==0)
    {
        clearcache();
        count = 0;
    }
    count++;

    /* always get local IP */
    memset(&ifr, 0, sizeof ifr);

    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    fcntl(sockfd, F_SETFL, fcntl(sockfd, F_GETFL) | O_NONBLOCK);

    char * local_IP = NULL;
    static char * local_IP_store = NULL;
    if(sockfd < 0)
    {
        close(sockfd);
        _asprintf(&local_IP, "%s", E_SOCKET);
    }
    else
    {
        strcpy(ifr.ifr_name, interface);
        sa->sin_family = AF_INET;

        if(0 > ioctl(sockfd, SIOCGIFADDR, &ifr))
        {
            close(sockfd);
            _asprintf(&local_IP, "%s", E_INTERFACE);
        }
        else
        {
            _asprintf(&local_IP, "%s", inet_ntoa(sa->sin_addr));
            close(sockfd);
        }
    }

    Dprint("local IP %s\n",local_IP);
    if(local_IP)
    {
        if(local_IP_store == NULL)
        {
            // store local IP
            local_IP_store = malloc(sizeof(char)*strlen(local_IP));
            strcpy(local_IP_store,local_IP);
        }
        else
        {
            Dprint("CF local_IP %s to store %s\n",
                    local_IP,
                    local_IP_store);
            if(strcmp(local_IP,local_IP_store)!=0)
            {
                // local IP changed : clean the cache
                Dprint("local IP changed\n");
                clearcache();
                strcpy(local_IP_store,local_IP);
            }
        }

        if(local_IP[0] == '-')
        {
            /* not connected */
            snprintf(buffer_,bufsz_,"disconnected");
            count = 0;
        }
        else if(n == 0)
        {
            // return local_IP in the buffer
            if(cache[0])free(cache[0]);
            _asprintf(&cache[0],"%s%s",labels[n],local_IP);
            strncpy(buffer_,
                    cache[0],
                    (size_t)BUFSIZE);
        }
        else
        {
            if(cache[n] == NULL)
            {
                /*
                 * no cached value
                 * return external command data in buffer
                 */
                char * extIP = external_data(commands[n]);
                if(extIP)
                {
                    snprintf(buffer_, bufsz_, "%s%s", labels[n], extIP);
                    if(cache[n])free(cache[n]);
                    _asprintf(&cache[n],"%s",buffer_);
                    free(extIP);
                }
            }
            else
            {
                /* use cached values if we have them */
                Dprint("retcache %s\n",cache[n]);
                strncpy(buffer_,
                        cache[n],
                        (size_t)BUFSIZE);
            }
        }
        free(local_IP);
    }

    return;
}

static void load_config(gchar *config_line)
{
    gchar	config_keyword[32], config_data[CFG_BUFSIZE];
    gint	n;
    gchar   temp[10];

    if((n = sscanf(config_line, "%50s %[^\n]", config_keyword, config_data)) < 2)
        return;

    if(strcmp(config_keyword, "interface") == 0)
        gkrellm_dup_string(&strg_iface, config_data);

    if(strcmp(config_keyword, "prefix") == 0)
        gkrellm_dup_string(&strg_prefix, config_data);

    if(strcmp(config_keyword, "show_iface") == 0) {
        sscanf(config_data, "%s", temp);
        int_iface = atoi(temp);
    }
    if(strcmp(config_keyword, "show_prefix") == 0) {
        sscanf(config_data, "%s", temp);
        int_prefix = atoi(temp);
    }
    if(strcmp(config_keyword, "do_scroll") == 0) {
        sscanf(config_data, "%s", temp);
        int_scroll = atoi(temp);
    }

    return;
}

static void save_config(FILE * f)
{
    fprintf(f, "%s interface %s\n", CONFIG_NAME, strg_iface);
    fprintf(f, "%s prefix %s\n",    CONFIG_NAME, strg_prefix);
    fprintf(f, "%s show_iface %d\n", CONFIG_NAME, int_iface);
    fprintf(f, "%s show_prefix %d\n", CONFIG_NAME, int_prefix);
    fprintf(f, "%s do_scroll %d\n", CONFIG_NAME, int_scroll);
    return;
}

static void apply_config()
{
    if(interface_entry)
        gtk_entry_set_text (GTK_ENTRY (interface_entry), _(strg_iface));

    if(prefix_entry)
        gtk_entry_set_text (GTK_ENTRY (prefix_entry), _(strg_prefix));

    if(int_iface)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (interface_btn), _(int_iface));

    if(int_prefix)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (prefix_btn), _(int_prefix));

    if(int_scroll)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (scroll_btn), _(int_scroll));

    return;
}

static gint panel_expose_event(GtkWidget *widget, GdkEventExpose *ev)
{

    gdk_draw_pixmap(widget->window, widget->style->fg_gc[GTK_WIDGET_STATE (widget)], panel->pixmap, ev->area.x, ev->area.y, ev->area.x, ev->area.y, ev->area.width, ev->area.height);
    return FALSE;
}

static void panel_button_event(GtkWidget *widget, GdkEventButton *event, gpointer data)
{
    if(event->button == 3)
        gkrellm_open_config_window(monitor);

    if(event->button == 1) {
        int_scroll = !int_scroll;
                gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (scroll_btn), int_scroll);
    }
}

void on_interface_btn_clicked     (GtkButton     *button,    gpointer user_data)
{
    int_iface = !int_iface;
}

void on_prefix_btn_clicked        (GtkButton     *button,    gpointer user_data)
{
    int_prefix = !int_prefix;
}

void on_scroll_btn_clicked        (GtkButton     *button,    gpointer user_data)
{
    int_scroll = !int_scroll;
}

void on_prefix_entry_changed      (GtkWidget *widget,  GtkWidget *entry)
{
    const gchar *entry_text;
    entry_text = gtk_entry_get_text (GTK_ENTRY (entry));
    gkrellm_dup_string(&strg_prefix, (gchar *) entry_text);
}

void on_interface_entry_changed   (GtkWidget *widget,  GtkWidget *entry)
{
    const gchar *entry_text;
    entry_text = gtk_entry_get_text (GTK_ENTRY (entry));
    gkrellm_dup_string(&strg_iface, (gchar *) entry_text);
}

static void create_plugin_tab(GtkWidget *tab_vbox)
{
  GtkWidget *notebook;
  GtkWidget *option_box;
  GtkWidget *interface_label;
  GtkWidget *prefix_label;
  GtkWidget *line;
  GtkWidget *tab_option;
  GtkWidget *about_label;
  GtkWidget *about_text;
  GtkWidget *info_box;
  GtkWidget *info_text;
  gchar     *text;
  gint      i;

  text = g_strdup_printf(
    PLUGIN_NAME " " PLUGIN_VERSION "\n"
    "GkrellM2 show local IP Plugin" "\n\n"
    "Copyright (c) 2007 by Detlef Wien\n"
    "DetlefWien@GMX.de\n\n"
    "Released under the GNU Public Licence.");


  notebook = gtk_notebook_new ();
  gtk_widget_show (notebook);
  gtk_container_add (GTK_CONTAINER (tab_vbox), notebook);
  gtk_notebook_set_scrollable (GTK_NOTEBOOK (notebook), TRUE);

  option_box = gtk_fixed_new ();
  gtk_widget_show (option_box);
  gtk_container_add (GTK_CONTAINER (notebook), option_box);

  prefix_entry = gtk_entry_new ();
  gtk_widget_show (prefix_entry);
  gtk_fixed_put (GTK_FIXED (option_box), prefix_entry, 168, 48);
  gtk_entry_set_max_length (GTK_ENTRY (prefix_entry), 10);
  gtk_entry_set_text (GTK_ENTRY (prefix_entry), _(strg_prefix));

  interface_entry = gtk_entry_new ();
  gtk_widget_show (interface_entry);
  gtk_fixed_put (GTK_FIXED (option_box), interface_entry, 168, 16);
  gtk_entry_set_max_length (GTK_ENTRY (interface_entry), 10);
  gtk_entry_set_text (GTK_ENTRY (interface_entry), _(strg_iface));

  interface_label = gtk_label_new (_("Interface (eth0, ppp0, ...):"));
  gtk_widget_show (interface_label);
  gtk_fixed_put (GTK_FIXED (option_box), interface_label, 16, 24);
  gtk_label_set_justify (GTK_LABEL (interface_label), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (interface_label), 0, 0.5);

  prefix_label = gtk_label_new (_("Prefix     (optional label):"));
  gtk_widget_show (prefix_label);
  gtk_fixed_put (GTK_FIXED (option_box), prefix_label, 16, 56);
  gtk_label_set_justify (GTK_LABEL (prefix_label), GTK_JUSTIFY_RIGHT);
  gtk_misc_set_alignment (GTK_MISC (prefix_label), 0, 0.5);

  line = gtk_hseparator_new ();
  gtk_widget_show (line);
  gtk_fixed_put (GTK_FIXED (option_box), line, 16, 92);
  gtk_widget_set_size_request (line, 312, 24);

  prefix_btn = gtk_check_button_new_with_mnemonic (_("show prefix"));
  gtk_widget_show (prefix_btn);
  gtk_fixed_put (GTK_FIXED (option_box), prefix_btn, 32, 168);
  gtk_widget_set_size_request (prefix_btn, 296, 32);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (prefix_btn), int_prefix);

  interface_btn = gtk_check_button_new_with_mnemonic (_("show interface name"));
  gtk_widget_show (interface_btn);
  gtk_fixed_put (GTK_FIXED (option_box), interface_btn, 32, 136);
  gtk_widget_set_size_request (interface_btn, 296, 32);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (interface_btn), int_iface);

  scroll_btn = gtk_check_button_new_with_mnemonic (_("scroll text"));
  gtk_widget_show (scroll_btn);
  gtk_fixed_put (GTK_FIXED (option_box), scroll_btn, 32, 200);
  gtk_widget_set_size_request (scroll_btn, 296, 32);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (scroll_btn), int_scroll);

  tab_option = gtk_label_new (_("Option"));
  gtk_widget_set_name (tab_option, "tab_option");
  gtk_widget_show (tab_option);
  gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), 0), tab_option);

  info_box = gkrellm_gtk_framed_notebook_page(notebook, "Info");
  info_text = gkrellm_gtk_scrolled_text_view(info_box, NULL, GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  for (i = 0; i < sizeof(plugin_info_text)/sizeof(gchar *); ++i)
    gkrellm_gtk_text_view_append(info_text, plugin_info_text[i]);

  about_text = gtk_label_new(text);
  about_label = gtk_label_new(_("About"));
  gtk_notebook_append_page(GTK_NOTEBOOK(notebook), about_text, about_label);
  g_free(text);

    g_signal_connect ((gpointer) prefix_btn, "clicked", G_CALLBACK (on_prefix_btn_clicked), NULL);
    g_signal_connect ((gpointer) interface_btn, "clicked", G_CALLBACK (on_interface_btn_clicked), NULL);
    g_signal_connect ((gpointer) scroll_btn, "clicked", G_CALLBACK (on_scroll_btn_clicked), NULL);
    g_signal_connect (G_OBJECT (prefix_entry), "changed", G_CALLBACK (on_prefix_entry_changed), (gpointer) prefix_entry);
    g_signal_connect (G_OBJECT (interface_entry), "changed", G_CALLBACK (on_interface_entry_changed), (gpointer) interface_entry);

    return;
}

#if GKRELLM_CHECK_VERSION(2,3,0)
gchar *
export_ip(gchar key, gchar *which)
{
    static gchar ip[BUFSIZE];
    read_ip(ip, BUFSIZE, which,0);
    return ip;
}
#endif

static void create_plugin(GtkWidget *vbox, gint first_create)
{
    GkrellmStyle		*style;
    GkrellmTextstyle	*ts;//, *ts_alt;

    if(first_create)
        panel = gkrellm_panel_new0();

    style = gkrellm_meter_style(style_id);

    ts = gkrellm_meter_textstyle(style_id);

//    ts_alt = gkrellm_meter_alt_textstyle(style_id); // unused

    decal_text = gkrellm_create_decal_text(panel,
            "init ...", ts, style, -1, -1, -1);

    gkrellm_panel_configure(panel, NULL, style);
    gkrellm_panel_create(vbox, monitor, panel);

    if(first_create) {
        g_signal_connect (G_OBJECT (panel->drawing_area), "expose_event", G_CALLBACK (panel_expose_event), NULL);
        g_signal_connect (GTK_OBJECT(panel->drawing_area), "button_release_event", (GtkSignalFunc) panel_button_event, NULL);
#if GKRELLM_CHECK_VERSION(2,3,0)
        gkrellm_plugin_export_label(monitor, "Net", 'A', export_ip);
#endif
    }
    return;
}

static void update_plugin()
{
    static gint		x_scroll;
    static gchar	ip[BUFSIZE];
    static gchar	info[40];
    gint			w_scroll, w_decal;
    static int callcount = 0;
    static char * commands[] = { COMMANDS_LIST };
    static const size_t n_commands = sizeof(commands)/sizeof(commands[0]);

    if(!int_prefix && !int_iface && strg_iface[0] == '\0')
    {
        gkrellm_panel_hide(panel);
        return;
    }
    else
    {
        gkrellm_panel_show(panel);
    }
    gkrellm_decal_get_size(decal_text, &w_decal, NULL);

    if(gticks->two_second_tick)
    {
        /* call read_ip */
        read_ip(ip, BUFSIZE, strg_iface,callcount);
        callcount++;
        if(callcount >= n_commands) callcount = 0;

        if(int_prefix && int_iface)
            snprintf(info, sizeof(info), "%s %s %s",
                     strg_prefix, strg_iface, ip);
        else if(int_prefix)
            snprintf(info, sizeof(info), "%s %s", strg_prefix, ip);
        else if(int_iface)
            snprintf(info, sizeof(info), "%s %s", strg_iface, ip);
        else
            snprintf(info, sizeof(info), "%s", ip);

        gkrellm_dup_string(&display_text, info);
    }

    gkrellm_decal_scroll_text_set_text(panel, decal_text, display_text);
    gkrellm_decal_scroll_text_get_size(decal_text, &w_scroll, NULL);

    if(int_scroll)
    {
        /* Start scrolling with text at 1/6 panel width position and scroll
           |  until right edge of text is 1/6 in from right panel edge.
        */
        x_scroll -= 1;
        if(x_scroll + w_scroll < w_decal - w_decal / 6)
            x_scroll = w_decal / 6;
    }
    else
        x_scroll = 0;

    gkrellm_decal_text_set_offset(decal_text, x_scroll, 0);
    gkrellm_draw_panel_layers(panel);

    return;
}

static GkrellmMonitor	plugin_mon = {

    CONFIG_NAME,		/* Name, for config tab */
    0,			/* Id,  0 if a plugin */
    create_plugin,		/* The create function */
    update_plugin,		/* The update function */
    create_plugin_tab,	/* create_plugin_tab() */
    apply_config,		/* Apply the config function */
    save_config,		/* Save user config  */
    load_config,		/* Load user config */
    CONFIG_NAME,		/* config keyword */
    NULL,			/* Undefined 2 */
    NULL,			/* Undefined 1 */
    NULL,			/* private */
    PLUGIN_PLACEMENT,	/* insert plugin before this monitor */
    NULL,			/* Handle if a plugin, filled in by GKrellM  */
    NULL			/* path if a plugin, filled in by GKrellM */
};

/* MAIN */

GkrellmMonitor * gkrellm_init_plugin()
{
    style_id = gkrellm_add_meter_style(&plugin_mon, STYLE_NAME);
    gticks = gkrellm_ticks();

    strg_iface = g_strdup("");
    strg_prefix = g_strdup("IP:");

    monitor = &plugin_mon;
    return &plugin_mon;
}

static char * external_data(char * command)
{
    /* get external data */
    const size_t buffersize = 100;
    char * const buffer = calloc(1,sizeof(char)*buffersize);
    char * cmd = NULL;
    _asprintf(&cmd,
              "%s%s",
              REMOTE_CURL_COMMAND,
              command);
    if(cmd)
    {
        Dprint("run %s\n",cmd);
        FILE * fp = popen(cmd, "r");
        free(cmd);
        if(fp)
        {
            /* Read the output a line at a time - output it. */
            while(fgets(buffer, buffersize, fp) != NULL)
            {
                pclose(fp);
                return buffer;
            }
        }
        pclose(fp);
    }
    free(buffer);
    return NULL;
}


int _asprintf(
    char **  const string_pointer,
    const char *  const format,
    ...)
{
    /*
     * binary_c's asprintf, to be used if libbsd or gnu's
     * asprintf is not available.
     *
     * Returns the number of bytes copied successfully,
     * just as sprintf (and friends) would.
     *
     * *string_pointer needs freeing by the calling function
     * to avoid a memory leak.
     */
    va_list args,args_copy;
    va_start(args,format);
    va_copy(args_copy,args);
    int len = vsnprintf(0,
                        0,
                        format,
                        args_copy);
    va_end(args_copy);
    if(len<0)
    {
        va_end(args);
        return len;
    }
    else
    {
        * string_pointer = malloc(sizeof(char)*(len+2));
        if(*string_pointer==NULL)
        {
            va_end(args);
            return 0;
        }
        else
        {
            len = vsnprintf(*string_pointer,
                            len+1,
                            format,
                            args);
            return len;
        }
    }

}

void clearcache(void)
{
    Dprint("clear cache\n");
    for(int i=0;i<NCOMMANDS;i++)
    {
        free(cache[i]);
        cache[i] = NULL;
    }
}
