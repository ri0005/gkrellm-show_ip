all: GKrellM2-show_ip.so

GKrellM2-show_ip.o: show_ip.c
	gcc -fPIC -O2 -Wall `pkg-config gtk+-2.0 --cflags` -c show_ip.c -o GKrellM2-show_ip.o

GKrellM2-show_ip.so: GKrellM2-show_ip.o
	gcc -shared -O2 -Wall -o GKrellM2-show_ip.so GKrellM2-show_ip.o

clean:
	rm -f *.o core *.bak *~

install:
	cp GKrellM2-show_ip.so ~/.gkrellm2/plugins/
