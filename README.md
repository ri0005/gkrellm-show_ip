GKrellM2 show ip (LAN and WAN)

This plugin shows the actual ip address of a given network interface.
  - Surprise! Did you expect that? ;)

Why use this plugin ?
If you are behind a firewall with NAT or in a LAN you can see your 
local and external IP address. 

This plugin shows the actual ip address of a given network interface.
- Surprise! Did you expect that? ;),
Why use this plugin ?,
If you are behind a firewall with NAT or in a LAN you can see youre,
local address (not the external IP). This is usefull too for,
wireless APs with DHCP enabled ...

Usage:
  Left clicking the plugin toggles the scroll.
  Right clicking the plugin opens the configuration.

Configuration:,
  Interface field,
    Enter in the interface entryfield the name of,
    the interface you want to watch.
    (eth0, eth1, irda0, ppp0, dsl0, wlan0, or any other ...),
  Prefix field,
    Enter in the prefix entryfield any string you,
    want to see in front of the displayed string.
    (one word, special chars may work but no spaces) ,
  Interface checkbox,
    Display the Interface name, or not.
  Prefix checkbox,
    Display the prefix, or not.
  Scroll text checkbox,
    Let the text scrolling, or not.
    Left clicking the plugin toggles the scroll too.
   IP Address Export:,
The IP address for net interfaces can be exported for display on,
a builtin Net monitor chart by entering the $A substitution variable,
in the Net->Setup 'Format String for Chart Labels',


The plugin requires GKrellM > 2.2.0.
If compiled under GKrellm >= 2.3.0, the ip address can be exported to
display on the builtin net monitor chart.  See the gkrellm config.
Plugins->show_ip->Info tab.

System requirements:
curl
timeout

To install these run:
```
sudo apt-get install curl coreutils
```

When exporting like this, the GkrellM2 show ip panel may be hidden,
by clearing the 'Interface' entry and unchecking the 'show interface,
name' and 'show prefix' check buttons.


To install in ~/.gkrellm2/plugins/, just run
```
make
make install
```


The plugin is based on the gkrellmip-plugin 
from Luksch Klaus-J. <cyberclaus@gmx.net>


Copyright 2007 by Detlef Wien <DetlefWien@GMX.de>, updates by Robert Izzard 
<rob.izzard@gmail.com>.

This program is free software; you can redistribute it and/or modify,
it under the terms of the GNU General Public License as published by,
the Free Software Foundation; either version 2 of the License, or,
(at your option) any later version.
This program is distributed in the hope that it will be useful,,
but WITHOUT ANY WARRANTY; without even the implied warranty of,
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the,
GNU General Public License for more details.
You should have received a copy of the GNU General Public License,
along with this program; if not, write to the Free Software,
Foundation, Inc. 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.

Modified by Robert Izzard to show, alternately, the LAN and WAN IP ,
addresses. Released under the above GNU GPL2.

Tested on Ubuntu 20.10.
